# Ressourcen für Editionen

Irgendwie alle Ressourcen die zu BBF-Editionen gehören:

- Schemata
- Node-Module für TEI Publisher

## Installation
Das `build`-Skript kopiert alle Dateien und verpackt sie, so daß sie in einer eXist-Datenbank installiert werden können.

### BBF-Editionen-Schema
Das Schema wird in einem anderen Repositorium gepflegt: https://gitlab.tba-hosting.de/bbf-editionen/bbf-editionen-schema/. Dort ist eine GitLab-CI-Pipeline konfiguriert, die bei jedem Commit in den `main`-Branch ausgelöst wird und RelaxNG- und HTML-Versionen erzeugt. Diese müssen derzeit noch (Stand Juni 2022) manuell in dieses Repositorium kopiert werden.
### TEI Publisher-Module
Die TEI Publisher-Module werden in diesem Repositorium gepflegt. Dazu gibt es die Datei `package.json`, die die benötigten Pakete auflistet. Zur Zusammenstellung der fertigen Pakete ist eine GitLab-CI-Pipeline in diesem Repositorium konfiguriert, die dann einen `npm`-Befehl ausführt und die fertigen Module als Artefakte in GitLab ablegt. Von dort werden sie manuell in dieses Repositorium kopiert. Das muß pro benutzter Version (Stand Juni 2022: 1.24.18) nur einmal gemacht werden.
#### Eigenentwicklungen
Um Veränderungen in TEI-Publisher-Modulen vorzunehmen und zu testen, gibt es zusätzlich `node-modules-dev`, worin Änderungen an den TEI Publisher-Modulen vorgenommen werden können. Zur Zeit werden die im Jahrbuch-Projekt eingesetzt.

## Pipeline zum Patchen
Die automatischen Änderungen werden durch ein `npm`-Paket erledigt: [https://www.npmjs.com/package/patch-package](patch-package).

### Einmaliger Aufwand
In einer installierten Version (also Dateien, die im `node_modules`-Verzeichnisbaum liegen) werden Änderungen gemacht. Mit dem `patch-package`-Paket werden dann Patch-Dateien erzeugt:

    npx @teipublisher/pb-components

Achtung: `patch-package` kann nicht mit Windows-Zeilenenden umgehen! Das heißt, entweder auf einem Server machen oder im WSL.


### Weiteres Patchen
Um weitere Patches zu erzeugen, wird auf einem Un*x-System dieses Repositorium ausgecheckt, und in `node_modules` (nicht `node_modules_dev`!) werden die erforderlichen Änderungen durchgeführt.

Auf der obersten Ebene der Arbeitskopie wird

    npx patch-package @teipublisher/pb-components

ausgeführt. Damit wird in einem temporären Verzeichnis eine saubere Version des angegebenen Pakets runtergeladen, installiert und mit der lokalen Version verglichen. Ein Patch wird dann im Verzeichnis `patches` abgelegt. Das ist eine Datei pro Paket. Spätere Patches müssen dann also manuell kopiert werden.

Leider müssen auch die Änderungen sowohl in der Quelldatei (z. B. `src/pb-view.js`) als auch in der kompilierten Datei (`dist/pb-components-bundle.js`) gemacht werden, weil der Patch wohl nur nach der Installation ausgeführt werden kann und nicht vorher.

Von Interesse sind vielleicht auch noch die genauen Versionen und Commits der installierten Pakete:
- tei-publisher-components: 1.24.18 (https://github.com/eeditiones/tei-publisher-components/commit/4392b602a27e2352faf8f3e85c2ca871098e97ad)
- swagger-ui: 3.52.5 (https://github.com/swagger-api/swagger-ui/commit/f1ad60dc92e7edb0898583e16c3e66fe3e9eada2)
